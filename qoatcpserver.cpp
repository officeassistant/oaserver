#include "qoatcpserver.h"

QOATCPserver::QOATCPserver(QObject *parent) : QObject(parent)
{
    Server = new QTcpServer();
    setObjectName(QString(tr("OATCPServer")));
    connect(Server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    Address = QHostAddress::Any;
    macs = Q_NULLPTR;
    Port = 9999;
    serverState = false;
    serverEnable = false;
    authEnable = true;
    lastError = QString();
}

QOATCPserver::~QOATCPserver()
{
    stop();
    delete Server;
}


quint16 QOATCPserver::getPort()
{
    return Port;
}


void QOATCPserver::listen()
{
    if (serverState == true) return;
    if (!Server->listen(Address, Port) && serverState == false) {
        return;
    }
    serverState = true;
}

void QOATCPserver::stop()
{
    if (serverState == false) return;

    for (int i = TCPClients.count() - 1; i >= 0; i --)
        TCPClients[i]->slotDisconnect();

    Server->close();
    serverState = false;

}

bool QOATCPserver::isEnabled()
{
    return serverEnable;
}

QString QOATCPserver::error()
{
    return lastError;
}

void QOATCPserver::setEnabled(bool enable)
{
    serverEnable = enable;
}

void QOATCPserver::setPort(quint16 port)
{
    quint16 oldPort = Port;
    Port = port;
    if (serverState && Port != oldPort) {
        stop();
        listen();
    }
}

bool QOATCPserver::isConnected()
{
    return serverState;
}

void QOATCPserver::newConnection()
{
    QOATCPSocket *tcpSocket = new QOATCPSocket(Server->nextPendingConnection(), this);
    TCPClients.append(tcpSocket);
    tcpSocket->macs = macs;
    connect(tcpSocket, SIGNAL(closeConnected(QOATCPSocket*)), this, SLOT(closeConnection(QOATCPSocket*)));

    stateChanged();
}

void QOATCPserver::closeConnection(QOATCPSocket *socket)
{
    for (int i = TCPClients.count() - 1; i >= 0; i --)
        if (TCPClients[i] == socket) TCPClients.removeAt(i);
    stateChanged();
}


//==================================================================================================

QOATCPSocket::QOATCPSocket(QTcpSocket *Socket, QObject *)
{
    setObjectName(QString(tr("TCP Server")));
    clientSocket = Socket;
    macs = Q_NULLPTR;
    caseMode = 1;
    connect(Socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(Socket, SIGNAL(disconnected()), this, SLOT(slotDisconnected()));
}

QOATCPSocket::~QOATCPSocket()
{
    disconnect(clientSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    disconnect(clientSocket, SIGNAL(disconnected()), this, SLOT(slotDisconnected()));
}


void QOATCPSocket::readyRead()
{
    switch (caseMode)
    {
    case 1:state_1(); break;
    case 2:state_2(); break;
    case 3:readSize(); break;
    case 4:readData(); break;
    }
}

void QOATCPSocket::slotDisconnected()
{
    closeConnected(this);
    this->deleteLater();
}

void QOATCPSocket::writeSize(quint32 size)
{
    QByteArray block;
    QDataStream stream(&block, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_4_7);
    stream << quint8(115);
    stream << quint8(110);
    stream << size;
    clientSocket->write(block);
    clientSocket->flush();
}

void QOATCPSocket::readSize()
{
    if (clientSocket->bytesAvailable() < 4) return;
    QByteArray ba;
    ba = clientSocket->read(4);
    QDataStream stream(&ba, QIODevice::ReadOnly);
    stream.setVersion(QDataStream::Qt_4_7);
    stream >> blockSize;
    caseMode ++;
    readyRead();
}

void QOATCPSocket::slotDisconnect()
{
    clientSocket->close();
}

void QOATCPSocket::readData()
{
    if (clientSocket->bytesAvailable() < static_cast<qint64>(blockSize)) return;
    QByteArray block;
    block = clientSocket->read(static_cast<qint64>(blockSize));
    QDataStream stream(&block, QIODevice::ReadOnly);
    stream.setVersion(QDataStream::Qt_4_7);
    QString mac;
    stream >> mac;
    macAnswer(mac);
    caseMode = 1;
    readyRead();
}

void QOATCPSocket::sendData(QByteArray *block)
{
    writeSize(static_cast<quint32>(block->size()));
    clientSocket->write(*block);
    clientSocket->flush();
}

void QOATCPSocket::state_1()
{
    if (clientSocket->bytesAvailable() < 1) return;
    char p;
    clientSocket->read(&p, 1);
    if (p == char(115)) caseMode ++;
    readyRead();
}

void QOATCPSocket::state_2()
{
    if (clientSocket->bytesAvailable() < 1) return;
    char p;
    clientSocket->read(&p, 1);
    if (p == char(110)) caseMode ++;
    else caseMode = 1;
    readyRead();
}

void QOATCPSocket::macAnswer(QString mac)
{
    QByteArray block;
    QDataStream stream(&block, QIODevice::ReadWrite);
    stream.setVersion(QDataStream::Qt_4_7);
    QString answer = "absents";
    for (int m = 0; m < macs->count(); m ++)
        if (mac == macs->at(m)) answer = "presents";
    stream << answer;
    sendData(&block);
}

