#ifndef QOATCPSERVER_H
#define QOATCPSERVER_H

#include <QCoreApplication>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDataStream>


class QOATCPSocket;

class QOATCPserver : public QObject
{
    Q_OBJECT
public:
    QList<QOATCPSocket *> TCPClients;

    explicit QOATCPserver(QObject *parent = Q_NULLPTR);
    ~QOATCPserver();
    quint16 getPort();
    bool isConnected();
    bool isEnabled();
    QString error();
    QList<QString> *macs;

signals:
    //---- For interface module
    void stateChanged();

public slots:
    //----BM
    void listen();
    void stop();
    void setEnabled(bool enable);
    void setPort(quint16 port);

private slots:
    void newConnection();
    void closeConnection(QOATCPSocket *socket);

private:
    QTcpServer *Server;
    QHostAddress Address;
    quint16 Port;
    bool authEnable;
    bool serverState;
    bool serverEnable;
    QString lastError;
};

class QOATCPSocket : public QObject
{
    Q_OBJECT
public:
    explicit QOATCPSocket(QTcpSocket *Socket, QObject *parent = Q_NULLPTR);
    ~QOATCPSocket();
    QList<QString> *macs;
    QTcpSocket* clientSocket;
    quint16 clientDevAddress();
    QHostAddress clientIPAddress();
    QString clientName();
    QDateTime connectionTime();
    QString clientLastError();

signals:
    //---- For interface module
    void closeConnected(QOATCPSocket *socket);

public slots:
    //----BM
    void slotDisconnect();

private slots:
    void readyRead();
    void slotDisconnected();


private:
    quint8 caseMode;
    quint32 blockSize;
    QString cName;
    QString cLastError;

    void writeSize(quint32 size);
    void readSize();
    void readData();
    void sendData(QByteArray *block);
    void state_1();
    void state_2();
    void macAnswer(QString mac);

};

#endif // QOATCPSERVER_H
