#include "oaserver.h"

oaserver::oaserver(QObject *parent) : QObject(parent)
{
    connect(&terminal, SIGNAL(readyRead()), this, SLOT(terminalRead()));
    connect(&timer, SIGNAL(timeout()), this, SLOT(check()));
    timer.setInterval(20000);
    tcpserver.listen();
    tcpserver.macs = &listMAC;
    debug = false;
    isRouterConnect = false;
//    timer.start();
    //    qDebug() << "oaserver start, check interval 20 second";
}

void oaserver::begin()
{
    qDebug() << QString("oaserver start, check interval %1 second").arg(timer.interval() / 1000);
    timer.start();
    check();
}

void oaserver::check()
{
    if (debug) qDebug() << "begin connect";
    terminal.disconnectFromHost();
    terminal.connectToHost(routerIP, 23);
}

void oaserver::terminalRead()
{
    QByteArray in;
    in = terminal.readAll();
    cleanLine(&in);
    QString line = QString(in);
    if (line.indexOf("MAC") > -1) fillMAC(line);
    cmdprocessed(line);
}

void oaserver::cmdprocessed(QString cmd)
{
    if (cmd.indexOf("Login:", 0, Qt::CaseInsensitive) > -1) {
        if (debug) qDebug() << "Enter login";
        terminal.write(QString("%1\r\n").arg(routerLogin).toLocal8Bit());
        state = 0;
    } else if (cmd.indexOf("Password:", 0, Qt::CaseInsensitive) > -1) {
        if (debug) qDebug() << "Enter password";
        terminal.write(QString("%1\r\n").arg(routerPassword).toLocal8Bit());
    } else if (cmd.indexOf("(config)>", 0, Qt::CaseInsensitive) > -1) {
        if (!isRouterConnect) {
            qDebug() << "Router connect OK";
            isRouterConnect = true;
        }
        if (debug) qDebug() << "Enter cmd";
        if (state == 0) terminal.write(QString("show ip arp\r\n").toLocal8Bit());
        else terminal.disconnectFromHost();
        state ++;
    }
}

//void oaserver::loadUsers() {
//    QDir dir;

//    QFile file(dir.absoluteFilePath("users.txt"));

//    if (file.open(QIODevice::ReadOnly)) {
//        users.clear();
//        QTextStream in(&file);
//        QString line;
//        while(!in.atEnd()) {
//            line = in.readLine();
//            QStringList fields = line.split(" ");
//            if (fields.count() >= 3) {
//                user u;
//                u.mac = fields[0];
//                u.ip = fields[1];
//                u.name = fields[2];
//                users.append(u);
//                if (debug) qDebug() << "user " << u.name;
//            }
//        }
//        file.close();
//    } else qDebug() << "No file users" << dir.absoluteFilePath("users.txt");
//}

void oaserver::cleanLine(QByteArray *data)
{
    QByteArray r;
    r.append(0x1B);
    r.append(0x5B);
    r.append(0x4B);
    int i = data->indexOf(r);
    while (i > -1) {
        data->remove(i, 3);
        i = data->indexOf(r);
    }
}

void oaserver::fillMAC(QString data)
{
    QStringList list = data.split(" ",QString::SkipEmptyParts);
    listMAC.clear();
    for (int i = 0; i < list.count(); i ++) if (list[i].count() == 17) listMAC.append(list[i]);
    if (debug) qDebug() << listMAC;
    compareMAC();
}

void oaserver::compareMAC()
{
//    loadUsers();
//    bool lmac, nmac;
//    for (int u = 0; u < users.count(); u ++) {
//        lmac = existLastMAC(users[u].mac);
//        nmac = existNewMAC(users[u].mac);
//        if (lmac && !nmac) {
//            blockWorkStation(users[u].ip);
//            qDebug() << QString(tr("User %1 missing")).arg(users[u].name);
//        }

//        if (!lmac && nmac) {
//            qDebug() << QString(tr("User %1 present")).arg(users[u].name);
//        }
//    }
//    users.clear();
//    lastMAC = listMAC;
}

bool oaserver::existLastMAC(QString MAC)
{
    for (int i = 0; i < lastMAC.count(); i ++)
        if (MAC == lastMAC.at(i)) return true;
    return false;
}

bool oaserver::existNewMAC(QString MAC)
{
    for (int i = 0; i < listMAC.count(); i ++)
        if (MAC == listMAC.at(i)) return true;
    return false;
}

//void oaserver::blockWorkStation(QString ip)
//{
//    QProcess console;

//    if (debug) qDebug() << "LockWorkStation";

//        console.start(QString("cmd /c wmic.exe /node:%1 /USER:%2 /PASSWORD:%3 process call create \"cmd /c rundll32 user32.dll LockWorkStation\"")
//                      .arg(ip).arg("stanislav").arg("granitwork"));

//   // console.start(QString("cmd /c wmic.exe /node:%1 process call create \"cmd /c rundll32 user32.dll LockWorkStation\"").arg(ip));
//    console.waitForReadyRead();
//        qDebug() << console.readAllStandardOutput();
//}
