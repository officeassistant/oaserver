#include <QCoreApplication>
#include <qcommandlineparser.h>
#include <qcommandlineoption.h>
#include <QTextCodec>
#include <QTranslator>
#include <QObject>
#include <QSettings>

#include "oaserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QTextCodec * codec  = QTextCodec::codecForName("utf-8");
    // QTextCodec *codec = QTextCodec::codecForName("cp1251");
    QTextCodec::setCodecForLocale(codec);
    QCoreApplication::setOrganizationName("SignalNet");
    QCoreApplication::setApplicationName("OAServer");
    QCoreApplication::setApplicationVersion(QLatin1String("0.1.2.0"));

    QCommandLineParser parser;
    parser.setApplicationDescription("TCP Repiter");
    parser.addHelpOption();
    parser.addVersionOption();
    oaserver server;

//        server.routerIP = "192.168.130.1";
//        server.routerLogin = "admin";
//        server.routerPassword = "pas$worD";
//        server.debug = true;

    QCommandLineOption targetDirectoryOptionA(QStringList() << "a" << "address",
                                              QCoreApplication::translate("main", "Sets the connection address router."),
                                              QCoreApplication::translate("main", "Router IP address"));
    parser.addOption(targetDirectoryOptionA);


    QCommandLineOption targetDirectoryOptionL(QStringList() << "l" << "login",
                                              QCoreApplication::translate("main", "Specify the login of the router."),
                                              QCoreApplication::translate("main", "Login"));
    parser.addOption(targetDirectoryOptionL);

    QCommandLineOption targetDirectoryOptionP(QStringList() << "p" << "password",
                                              QCoreApplication::translate("main", "Specify the password of the router."),
                                              QCoreApplication::translate("main", "Password"));
    parser.addOption(targetDirectoryOptionP);

    QCommandLineOption targetDirectoryOptionD(QStringList() << "d" << "debug",
                                              QCoreApplication::translate("main", "Debug."),
                                              QCoreApplication::translate("main", "Debug"));
    parser.addOption(targetDirectoryOptionD);

    parser.process(a);

    QSettings settings;

    server.routerIP = settings.value("routerIP", "").toString();
    server.routerLogin = settings.value("routerLogin", "").toString();
    server.routerPassword = settings.value("routerPassword", "").toString();

    if (parser.isSet(targetDirectoryOptionA)) server.routerIP = parser.value(targetDirectoryOptionA);
    if (parser.isSet(targetDirectoryOptionL)) server.routerLogin = parser.value(targetDirectoryOptionL);
    if (parser.isSet(targetDirectoryOptionP)) server.routerPassword = parser.value(targetDirectoryOptionP);
    if (parser.isSet(targetDirectoryOptionD)) server.debug = true;

    if (!server.routerIP.isEmpty() && !server.routerLogin.isEmpty() && !server.routerPassword.isEmpty()) {
        settings.setValue("routerIP", server.routerIP);
        settings.setValue("routerLogin", server.routerLogin);
        settings.setValue("routerPassword", server.routerPassword);
        server.begin();
    } else qDebug() << "ERROR settings!!!";



    return a.exec();
}
