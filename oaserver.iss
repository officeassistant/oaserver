   ; ��� ����������
#define   Name       "OAServer"
; ������ ����������
#define   Version    "0.1.2.0"
; �����-�����������
#define   Publisher  "SignalNet"

[Setup]
AppId={{228F6294-B721-4424-983A-2D39C03B19B9}
AppName={#Name}
AppVersion={#Version}
DefaultDirName={pf}\SignalNet\oaserver
DefaultGroupName=SignalNet
;UninstallDisplayIcon=favicon.ico
;SetupIconFile=favicon.ico
Compression=lzma2
SolidCompression=yes
OutputDir=..\!Distrib
OutputBaseFilename=SetupOAServer {#Version}
AllowNoIcons=yes
;AppPublisherURL=http://signalnet.com
AppPublisher=SignalNet, LLC.
CreateUninstallRegKey=yes 

[Languages]
Name: "ru"; MessagesFile: "compiler:Languages\Russian.isl"

[Files]
Source: "..\build-oaserver-Desktop_Qt_5_12_0_MinGW_64_bit-Release\release\oaserver.exe"; DestDir: "{app}"

Source: "C:\Qt\5.12.0\mingw73_64\bin\libgcc_s_seh-1.dll"; DestDir: "{app}"
Source: "C:\Qt\5.12.0\mingw73_64\bin\libstdc++-6.dll"; DestDir: "{app}"
Source: "C:\Qt\5.12.0\mingw73_64\bin\libwinpthread-1.dll"; DestDir: "{app}"
Source: "C:\Qt\5.12.0\mingw73_64\bin\Qt5Core.dll"; DestDir: "{app}"
Source: "C:\Qt\5.12.0\mingw73_64\bin\Qt5Network.dll"; DestDir: "{app}"
Source: "C:\Qt\5.12.0\mingw73_64\plugins\platforms\qwindows.dll"; DestDir: "{app}\platforms"


[Icons]
Name: "{group}\oaserver"; Filename: "{app}\oaserver.exe"
